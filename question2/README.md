# Query Metadata

This is a script to display metadata from a running GCP instance, optionally filtering by key.

## Usage

```bash
./query-metadata.py project zone name [key]
```

For example

```bash
./query-metadata.py dev-project europe-west1-c test-instance kind
```

Would output

```
compute#metadata
```

If the key is invalid (does not appear in the metadata), the whole structure will be output