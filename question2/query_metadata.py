#!/usr/bin/env python3
import googleapiclient.discovery
import json
import argparse
import os

parser = argparse.ArgumentParser()
parser.add_argument('project', nargs=1, help='Which project to use')
parser.add_argument('zone', nargs=1, help='Which zone to use')
parser.add_argument('name', nargs=1, help='Which instance name to use')
parser.add_argument('key', nargs='?', help='Which key to display')
args = parser.parse_args()

os.environ["SUPPRESS_GCLOUD_CREDS_WARNING"] = "true"

compute = googleapiclient.discovery.build('compute', 'v1')
instance = compute.instances().get(project=args.project[0], zone=args.zone[0], instance=args.name[0]).execute()

metadata = instance['metadata']

if args.key and args.key in metadata:
    metadata = metadata[args.key]
    
print(json.dumps(metadata))