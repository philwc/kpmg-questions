#!/usr/bin/env bash
TEST_CASES="./testcases.csv"
while IFS=, read -r OBJECT KEY EXPECTED; do

    echo -n "$OBJECT, $KEY, $EXPECTED "

    ACTUAL=$(./extract_key.py $OBJECT $KEY)

    if [ "$EXPECTED" == "$ACTUAL" ]; then
        echo -e "\e[92m[OK]\e[39m"
    else
        echo -e "\e[91m[FAIL]\e[39m"
    fi
done <"$TEST_CASES"
