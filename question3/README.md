# Extract Key

This is a small script to recursively extract key parts from a JSON object.

## Usage

```bash
./extract_key.py '{"a":{"b":{"c":"d"}}}' 'a/b/c'
```

## Tests

Run the test suite:

```bash
./tests.sh
```

This will import testcases from `testcases.csv` and test the output of the script.