#!/usr/bin/env python3

import json
import argparse
import sys

parser = argparse.ArgumentParser()
parser.add_argument('object', nargs=1, help='JSON string to load')
parser.add_argument('key', nargs=1, help='Key to extract')

args = parser.parse_args()

object = args.object[0]
key = args.key[0]

try:
    data = json.loads(object)
except ValueError:
    print ('Unable to decode JSON: '+object)
    sys.exit(1)

keyparts = key.split('/')

for part in keyparts:
    if part in data:
        data = data[part]
    else:
        break

print(json.dumps(data))