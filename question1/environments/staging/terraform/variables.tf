variable subnetwork {
  default = "default"
}

variable project {
  default = "staging-project"
}

variable region {
  default = "europe-west1"
}

variable name {
  default = "test-instance"
}

variable machine_type {
  default = "n1-standard-1"
}

variable zone {
  default = "europe-west1-c"
}

variable allow_stop_for_updates {
  default = false
}

variable minimum_cpu_platform {
  default = "Intel Skylake"
}

variable network_tags {
  type = "list"

  default = []
}

variable labels {
  default = {}

  # default = {
  #   KEY = "VALUE"
  # }
}

variable delete_disk_on_termination {
  default = true
}

variable base_image {
  default = "ubuntu-2004-lts"
}

variable bootdisk_type {
  default = "pd-ssd"
}

variable bootdisk_size {
  default = "25"
}

variable permission_scopes {
  type = "list"

  default = [
    "cloud-platform",
  ]
}

variable preemptible {
  default = false
}
