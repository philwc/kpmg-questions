# Set up provider
provider "google" {
  project = "${var.project}"
  region  = "${var.region}"
}

# Set a static external IP for standard instance
resource "google_compute_address" "static-ip-address" {
  name = "${var.name}-static-ip"
}

# Set a static internal IP for standard instance
resource "google_compute_address" "static-internal-ip-address" {
  name         = "${var.name}-static-int-ip"
  subnetwork   = "${var.subnetwork}"
  address_type = "INTERNAL"
}

# Compute instance
resource "google_compute_instance" "instance" {
  name                      = "${var.name}"
  machine_type              = "${var.machine_type}"
  zone                      = "${var.zone}"
  allow_stopping_for_update = "${var.allow_stop_for_updates ? "1" : "0"}"
  min_cpu_platform          = "${var.minimum_cpu_platform}"

  tags   = "${var.network_tags}"
  labels = "${var.labels}"

  boot_disk {
    auto_delete = "${var.delete_disk_on_termination}"

    initialize_params {
      image = "${var.base_image}"
      type  = "${var.bootdisk_type}"
      size  = "${var.bootdisk_size}"
    }
  }

  network_interface {
    subnetwork = "${var.subnetwork}"
    network_ip = "${google_compute_address.static-internal-ip-address.address}"

    access_config {
      nat_ip = "${google_compute_address.static-ip-address.address}"
    }
  }

  service_account {
    scopes = "${var.permission_scopes}"
  }

  # Decide if we want a pre-emptable instance or standard based on conditional
  scheduling {
    on_host_maintenance = "${var.preemptible ? "TERMINATE" : "MIGRATE"}"
    preemptible         = "${var.preemptible ? "true" : "false"}"
    automatic_restart   = "${var.preemptible ? "false" : "true"}"
  }
}