# Environment Setup

It is intended that the user will operate with separate service accounts per environment, to prevent cross-contamination.

## Usage

1. Set environment variables to point to correct service account credentials
   `export GOOGLE_APPLICATION_CREDENTIALS=~/dev-creds.json`
2. Navigate to the correct directory for the env
   `cd environments/dev/terraform`
3. Run terraform
   ```bash
   terraform init
   terraform plan
   terraform apply
   ```